session:setAutoHangup(false)

local callid_number = session:getVariable("destination_number")
local S1,S2
local uuid =  session:getVariable("uuid");

freeswitch.consoleLog("notice", "uuid :: " ..uuid.. "\n")
local session_id = session:getVariable("session_id");
local path
local time = os.date("%Y%m%d")
local sounds = session:getVariable("sounds_dir")
local path_mp3 = sounds .. "/user/response_to_send_message.mp3"

--获取随机数种子
math.randomseed(tostring(os.time()):reverse():sub(1,6))

--/cloudcall_file/rec_20051/freeswitchRecordFile/
--/cloudcall_file/rec_20050/freeswitchRecordFile/
if(math.random(1,2) == 2)
then
	path="/cloudcall_file/rec_20051/freeswitchRecordFile/" ..time..""
else
	path="/cloudcall_file/rec_20050/freeswitchRecordFile/"	..time..""
end

--011356924075515692418157
callid_number = string.sub(callid_number,3)
S1 = string.sub(callid_number,1,11)
S2 = string.sub(callid_number,12)

freeswitch.consoleLog("notice", "S1 :: " ..S1.. "\n")
freeswitch.consoleLog("notice", "S2 :: " ..S2.. "\n")

session:execute("set","hangup_after_bridge=true")
session:execute("set","continue_on_fail=true")
session:execute("export","session_id=" ..uuid.. "")

session:execute("set","RECORD_TITLE=Recording " ..session:getVariable("destination_number").. " " ..callid_number.. " " ..os.date("%Y-%m-%d %H:%M").. "")
session:execute("set","RECORD_COPYRIGHT=(c)2011")
session:execute("set","RECORD_SOFTWARE=FreeSWITCH")
session:execute("set","RECORD_ARTIST=FreeSWITCH")
session:execute("set","RECORD_COMMENT=FreeSWITCH")
session:execute("set","RECORD_DATE=" ..os.date("%Y-%m-%d %H:%M").. "")
session:execute("set","RECORD_STEREO=true")
session:execute("export","execute_on_answer=record_session "..path.. "/" ..uuid.. ".wav")

session2 = freeswitch.Session("{origination_caller_id_number="..S1..",origination_caller_id_name="..S1.."}sofia/internal/"..S2.."@122.13.6.112:5060")
--192.168.81.1:60010
freeswitch.bridge(session,session2)

session:execute("pre_answer")
session:execute("playback", path_mp3)
session:execute("set","sip_h_send_message=true")
session:hangup()



