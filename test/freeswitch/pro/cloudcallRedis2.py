#!/usr/bin/env python
# coding: utf-8

from rediscluster import client
import demjson
import random
import chardet

#此运行在python2 上面，编码方面不太一样
###


class RedisClusterClient(object):
    instance = None

    def __init__(self, conn_list):
        self.conn_list = conn_list
        RedisClusterClient.instance = self.connect()

    def connect(self):
        """
        init connection
        :return: object
        """
        try:
            # 非密码连接redis集群
            # redisconn = StrictRedisCluster(startup_nodes=self.conn_list)
            # 使用密码连接redis集群
            redisconn = client.RedisCluster(startup_nodes=self.conn_list)
            return redisconn
        except Exception as e:
            print(e)
            return False

    def get_state(self):
        """
        :return:
        """
        res = RedisClusterClient(self.conn_list).connect()
        if not res:
            return False

        dic = res.cluster_info()

        for i in dic:
            ip = i.split(":")[0]
            if dic[i].get('cluster_state'):
                print("status, ip: ", ip, "value: ", dic[i].get('cluster_state'))

    def get_cop_show_num_dict(self, user_id):
        key = "copShowNumMap:%s" % user_id
        return RedisClusterClient.instance.hgetall(name=key)

    def get_cop_num(self, user_id, city=None):
        b_show_num_dict = self.get_cop_show_num_dict(user_id)
        if b_show_num_dict is None:
            return None
        if city is None:
            """
            direct random one if without city param
            """
            if len(b_show_num_dict) > 0:
                rl = list(b_show_num_dict.keys())
                return random.choice(rl)
        else:
            """
            random one in a city showNum list
            """
            show_num_list = [x for x, y in b_show_num_dict.items() if y is city]
            if len(show_num_list) > 0:
                return random.choice(show_num_list)

        return None

    def get_last_call_info(self, key):
        b_last_call_info_dict = RedisClusterClient.instance.hgetall(key)


nodes = [
    {'host': '172.16.200.35', 'port': 6379},
    {'host': '172.16.200.36', 'port': 6379},
    {'host': '172.16.200.37', 'port': 6379},
    {'host': '172.16.200.38', 'port': 6379},
    {'host': '172.16.200.39', 'port': 6379},
    {'host': '172.16.200.40', 'port': 6379}
]

# init
rdc = RedisClusterClient(nodes)

if __name__ == '__main__':
    print(rdc.get_cop_show_num_dict('20358'))

    # rdc.get_cop_num('20358')
    # rdc.get_last_call_info('LastCallInfo:02023398853:13724059427')
    # print("none city:"+rdc.get_cop_num('20358'))
    print(rdc.get_cop_show_num_dict('20358'))
    s1 = rdc.get_cop_num('20358', '2')
    # UTF-8 --> decode 解码 --> Unicode
    # Unicode --> encode 编码 --> GBK / UTF-8
    s3 = s1.decode('utf-8')
    print(s3)
    s2 = s1.encode('utf-8')
    print("with city:" + rdc.get_cop_num('20358', '2'))
    print("with city: %s" % type(s1))
    # a = 20358
    # b = 1
    # rdc.get_cop_num('20358')
    # rdc.get_last_call_info('LastCallInfo:02023398853:13724059427')