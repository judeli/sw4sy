--API对象
local api = freeswitch.API();
--订阅answer事件
local con = freeswitch.EventConsumer()
con:bind("CHANNEL_ANSWER")


--一个循环,相当于一个监听器
while (1 == 1)
do
    --这个是为了可以手动关闭此脚本,当想要关闭此脚本时候,运行另一个脚本就可以了
    local isDown = api:executeString("hash select/testEvent/shutdown");
    if ("shutdown" == isDown)
    then
        api:executeString("hash delete/testEvent/shutdown");
        freeswitch.consoleLog("info", "EventListener is shutdown now");
        break
    end

    --取出event,阻塞方法
    local eventAnswer = con:pop(1)

    --取出本次通话的UUID
    local uuid = eventAnswer:getHeader("Channel-Call-UUID");

    --取出被叫号码
    local targetNumber = eventAnswer:getHeader("Caller-Destination-Number");


    --如果被叫号码为12位例如(918819443392),那就是被叫接通的状态,假如是7918819443392,13位,那就是主叫接通状态

    if (string.len(targetNumber) == 12)
    then

        --用uuid从内存中取出本次通话主叫号码
        local callerNumber = api:executeString("uuid_getvar " .. uuid .. " caller_id_number");

        freeswitch.consoleLog("info", "callerNumber is" .. callerNumber)

        --给主叫发送被叫已经接通的message
        api:executeString("chat sip|eventListener@172.16.200.60|" .. callerNumber .. "@172.16.200.60|callee is answer");

    end


    --local callerAddr=api:executeString("uuid_getvar "..uuid.." network_addr");

    --[[freeswitch.consoleLog("info","targetNumber is "..targetNumber);
    freeswitch.consoleLog("info",uuid);
    freeswitch.consoleLog("info",callerNumber);
    freeswitch.consoleLog("info",callerChannelName);
    freeswitch.consoleLog("info",callerAddr);
    freeswitch.consoleLog("info",destinationNumber);]]


end