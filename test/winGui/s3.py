import win32gui, win32api, win32con, time
from PIL import ImageGrab
from aip import AipOcr
import numpy as np

# python3 遍历windows下 所有句柄及窗口名称
hwnd_title = dict()


# hwnd = win32gui.FindWindow("ChatWnd", "标题")  # 获取窗口
# win32gui.SetForegroundWindow(hwnd)  # 激活窗口
def get_all_hwnd(hwnd, mouse):
    if win32gui.IsWindow(hwnd) and win32gui.IsWindowEnabled(hwnd) and win32gui.IsWindowVisible(hwnd):
        hwnd_title.update({hwnd: win32gui.GetWindowText(hwnd)})


# parent为父窗口句柄id
def get_child_windows(parent):
    '''
    获得parent的所有子窗口句柄
     返回子窗口句柄列表
     '''
    if not parent:
        return
    hwndChildList = []
    win32gui.EnumChildWindows(parent, lambda hwnd, param: param.append(hwnd), hwndChildList)
    return hwndChildList


win32gui.EnumWindows(get_all_hwnd, 0)

sw4sy = None
pycharmHd = None
for h, t in hwnd_title.items():
    if t is not "":
        print(h, t)
        if t.startswith("神武4手游"):
            sw4sy = h
        if t.rfind("PyCharm", 0, len(t)) > 0:
            pycharmHd = h

# win32gui.HideCaret(pycharmHd)

# 激活窗口
win32gui.SetForegroundWindow(sw4sy)

# 获取子窗口
l = get_child_windows(sw4sy)
if l is not None:
    print(l)

# 获取标题
title = win32gui.GetWindowText(sw4sy)  # jbid为句柄id
print("标题：%s" % title)
# 获取类名: GLFW30
clsname = win32gui.GetClassName(sw4sy)
print("类名：%s" % clsname)

# 分别为左、上、右、下的窗口位置
left, top, right, bottom = win32gui.GetWindowRect(sw4sy)
print("窗口位置：%d,%d,%d,%d" % (left, top, right, bottom))

# 移动窗口到左上角
# 没有直接修改窗口大小的方式，但可以曲线救国，几个参数分别表示句柄,起始点坐标,宽高度,是否重绘界面
# ，如果想改变窗口大小，就必须指定起始点的坐标，没果对起始点坐标没有要求，随便写就可以；如果还想要放在原先的位置，
# 就需要先获取之前的边框位置，再调用该方法即可
win32gui.MoveWindow(sw4sy, 0, 0, right - left, bottom - top, True)

left, top, right, bottom = win32gui.GetWindowRect(sw4sy)
print("移动后窗口位置：%d,%d,%d,%d" % (left, top, right, bottom))

# 根据句柄进行点击操作
# 根据横纵坐标定位光标
# win32api.SetCursorPos([0, 0])
# 给光标定位的位置进行单击操作（若想进行双击操作，可以延时几毫秒再点击一次）
# win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP | win32con.MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0)
# 给光标定位的位置进行右击操作
# win32api.mouse_event(win32con.MOUSEEVENTF_RIGHTUP | win32con.MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0)
# win32gui.PostMessage(sw4sy, win32con.WM_LBUTTONDOWN, win32con.MK_LBUTTON, 0)

width = win32api.GetSystemMetrics(win32con.SM_CXSCREEN)
height = win32api.GetSystemMetrics(win32con.SM_CYSCREEN)
print("显示器屏幕大小：%d,%d" % (width, height))

# 发送关闭窗口消息
# win32gui.PostMessage(sw4sy, win32con.WM_CLOSE)

# 子窗口
for l_whd in l:
    title = win32gui.GetWindowText(l_whd)  # jbid为句柄id
    print("标题：%s" % title)
    # 获取标题
    clsname = win32gui.GetClassName(l_whd)
    # 获取类名: GLFW30
    print("类名：%s" % clsname)


# 鼠标点击
def click(whnd, x, y):
    sx, sy = win32gui.ScreenToClient(whnd, (x, y))
    # lParam = win32api.MAKELONG(x-10, y-30)
    lParam = win32api.MAKELONG(sx, sy)
    # MOUSEMOVE event is required for game to register clicks correctly
    win32gui.PostMessage(whnd, win32con.WM_MOUSEMOVE, 0, lParam)
    time.sleep(0.005)
    win32gui.PostMessage(whnd, win32con.WM_LBUTTONDOWN, win32con.MK_LBUTTON, lParam)
    time.sleep(0.005)
    win32gui.PostMessage(whnd, win32con.WM_LBUTTONUP, win32con.MK_LBUTTON, lParam)


# 任务
click(sw4sy, 379, 70)  # 391,104
time.sleep(1)
# 师门
click(sw4sy, 383, 248)  # 393,283
time.sleep(1)
click(sw4sy, 1140, 345)
time.sleep(1)
click(sw4sy, 382, 182)

# posx,posy = win32gui.GetCursorPos()
# print("屏幕鼠标位置：%d,%d" % (posx, posy))


# 截图任务栏（1149,192，1382-1149，592-192）的师门任务，

time.sleep(3)
# 参数说明
# 第一个参数 开始截图的x坐标
# 第二个参数 开始截图的y坐标
# 第三个参数 结束截图的x坐标
# 第四个参数 结束截图的y坐标
bbox = (1149, 192, 1382, 609)
i = ImageGrab.grab(bbox)
i.save("I:\\pythonSolution\\projects\\sw4sy\\test\\winGui\\tmp.png")

i = open(r'tmp.png', 'rb')
img = i.read()

# 识别图片文字,还是比较准确的，赞
APPP_ID = '18268607'
API_KEY = 'qwzaUW2z6NgjVNNlKYiXgPpX'
SECRET_KEY = 'Cs75lIXTx8WWGRuIWxf1ghnNYugQ3E0L'

client = AipOcr(APPP_ID, API_KEY, SECRET_KEY)
message = client.basicGeneral(img)
print("message:" + str(message))

taskIndex = 0
taskType = None
for msg in message.get('words_result'):
    word = msg.get('words')
    print(word)
    if word.startswith("师门"):
        taskType = word[3:6]
        taskIndex = word[8]
        print(f'任务类型：{taskType},任务环数:{taskIndex}')


# 检测当前任务类型:
# 师门-收集物资(1/10)
# 找到蛇胆酒
def case1():
    print('This is the 收集物资')


def case2():
    print('This is the 勇者无敌,打架')
    click(sw4sy, 1249, 234)


# 备战久黎（药，系统装备）
def case3():
    print('This is the 买酒、药')


def case4():
    print('This is the 买宠物')


# 备战久黎（药，系统装备）
def case5():
    print('This is the 买系统药，装备')
    click(sw4sy, 964, 644)
    click(sw4sy, 964, 644)


# 三大兽王
def case6():
    print('This is the 拜访')


def default():  # 默认情况下执行的函数
    print('No such ')


switch = {'收集物资': case1,  # 注意此处不要加括号
          '勇者无敌': case2,  # 注意此处不要加括号
          '捕捉宠物': case3,  # 注意此处不要加括号
          '寻找物品': case5,  # 注意此处不要加括号
          '捕捉宠物': case3,  # 注意此处不要加括号
          }

choice = taskType  # 获取选择
switch.get(choice, default)()  # 执行对应的函数，如果没有就执行默认的函数
