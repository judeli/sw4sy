import sys


class test(object):
    _var = {}
    __var1 = [1]
    var2 = [2]

    def __init__(self):
        self.attr_a = 'wws'
        self.attr_b = 'bbw'

    def __private_method(self):
        self.attr_c = self.attr_a + self.attr_b
        print("c:" + self.attr_c)

    def _show_attr_c(self):
        print("c:", self.attr_c)

    def ju_he_abc(self):
        self.__private_method()
        test._var['a'] = self.attr_a
        test._var['b'] = self.attr_b
        test._var['c'] = self.attr_c


class test_1(test):

    __slots__ = ['attr_c']

    def __init__(self):
        self.attr_a = '1-wws'
        self.attr_b = '1-bbw'

    def show_father_method(self):
        print("test1 show father c")
        self._show_attr_c()



def tttf(a, b):
    return a + b


if __name__ == '__main__':
    for i in range(10):
        print(tttf(i, 1000))

    t = test()
    t.ju_he_abc()
    print(test._var)

    t1 = test_1()
    t1.attr_c = "ccc"
    t1.show_father_method()
    print(test_1._var)

    print(test.__var1)
    print(test_1.__var1)