import pycurl
from io import BytesIO

buffer = BytesIO()
c = pycurl.Curl()
c.setopt(c.URL, 'https://aicall.ihk.cn:9443/fsm/external/badCustomerPhone/checkBadPhone?customerPhone=13724059427')
c.setopt(pycurl.SSL_VERIFYPEER, 1)
c.setopt(pycurl.SSL_VERIFYHOST, 2)
#<TIPS>windows 要指定证书的路径不然会出现(77, "SSL: can't load CA certificate file E:\\curl\\ca-bundle.crt")
#证书路径就在curl下载的压缩包里面。mac/linux下面可以注释掉。
c.setopt(pycurl.CAINFO, "I:\\javaSolution\ssl\\cafile.pem")
c.setopt(c.WRITEDATA, buffer)
c.perform()
c.close()
body = buffer.getvalue()
# Body is a byte string.
# We have to know the encoding in order to print it to a text file
# such as standard output.
print(body.decode('utf8'))
# print(body)