import demjson
import pycurl
from io import BytesIO

try:
    # python 3
    from urllib.parse import urlencode
except ImportError:
    # python 2
    from urllib import urlencode

buffer = BytesIO()
c = pycurl.Curl()
url = "https://aicall.ihk.cn:9443/fsm/external/badCustomerPhone/checkBadPhone"
c.setopt(c.URL, url)
c.setopt(pycurl.SSL_VERIFYPEER, 1)
c.setopt(pycurl.SSL_VERIFYHOST, 2)
#<TIPS>windows 要指定证书的路径不然会出现(77, "SSL: can't load CA certificate file E:\\curl\\ca-bundle.crt")
#证书路径就在curl下载的压缩包里面。mac/linux下面可以注释掉。
c.setopt(pycurl.CAINFO, "I:\\javaSolution\ssl\\cafile.pem")
post_data = {'customerPhone': '13724059427'}
# Form data must be provided already urlencoded.
postfields = urlencode(post_data)
# Sets request method to POST,
# Content-Type header to application/x-www-form-urlencoded
# and data to send in request body.
c.setopt(c.POSTFIELDS, postfields)
c.setopt(c.WRITEDATA, buffer)
c.perform()
c.close()
body = buffer.getvalue()
# Body is a byte string.
# We have to know the encoding in order to print it to a text file
# such as standard output.
rs = body.decode('utf8')
if rs is not None:
    rs = demjson.decode(rs)
print(rs)
print(rs['msg'])
print(type(rs['code']))
