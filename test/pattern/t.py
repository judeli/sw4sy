# -*- coding:UTF8 -*-

import re

pattern = re.compile(r'\d+')  # 查找数字
result1 = pattern.findall('runoob 123 google 456')
result2 = pattern.findall('run88oob123google456', 0, 10)

print(result1)
print(result2)

pattern = re.compile(r'\d+')  # 查找数字
result1 = pattern.findall('师门-收集物资(10/10)')
result2 = pattern.findall('师门-收集物资10/10)', 0, 11)

print(result1[0])
print(result2)

theList = ['a','b','c']
if 'a' in theList:
    print('a in the list')

if 'd' not in theList: 
    print('d is not in the list')