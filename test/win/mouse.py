import autopy


# autopy.mouse.move(90, 10)
# autopy.mouse.smooth_move(90, 10)
# autopy.mouse.click()

def mouseMove(x, y):
    autopy.mouse.smooth_move(x, y)


def mouseClick():
    autopy.mouse.click()

def mouseDoubleClick():
    autopy.mouse.click()


def hello_world():
    autopy.alert.alert("Hello, world")


# hello_world()



import math
import time
import random

TWO_PI = math.pi * 2.0


def sine_mouse_wave():
    '''
    让鼠标以正弦波的形式从屏幕左边移到右边
    '''
    width, height = autopy.screen.size()
    height /= 2
    height -= 10  # 留在屏幕边界
    for x in range(int(width)):
        y = int(height * math.sin((TWO_PI * x) / width) + height)
        autopy.mouse.move(x, y)
        time.sleep(random.uniform(0.001, 0.003))


# sine_mouse_wave()


def where_is_the_rubbish():
    """查找回收站在屏幕中的位置"""
    rubbish = autopy.bitmap.Bitmap.open('dirsw4.png')
    screen = autopy.bitmap.capture_screen()
    pos = screen.find_bitmap(rubbish)
    if pos:
        print('找到了，他的位置在:%s' % str(pos))
    else:
        print('没有找到')


# where_is_the_rubbish()
