from PIL import Image
import PIL.ImageChops as imagechops
from PIL import Image, ImageDraw, ImageFont
import random


def CalcDistance(srcImg, distImg):
    im1 = Image.open(srcImg)
    im2 = Image.open(distImg)
    # 得出两图不一致的地方
    diff = imagechops.difference(im1, im2)
    draw = ImageDraw.Draw(diff)
    # 通过颜色处理清除干扰块
    for x in range(0, 260):
        for y in range(0, 116):
            pixelColor = diff.getpixel((x, y))
            if pixelColor[0] >= 100 or pixelColor[1] >= 100 or pixelColor[2] >= 100:
                draw.line((x, y, x, y), (255, 255, 255, 255))
            else:
                draw.line((x, y, x, y), (0, 0, 0, 0))
    # #清理完可以show一下，查看清理完之后的黑白化效果
    # diff.show()
    # 找第一个块中的参照点
    firsetPoint = [0, 0]
    for x in range(0, 260):
        for y in range(0, 116):
            pixelColor = diff.getpixel((x, y))
            if pixelColor != (0, 0, 0, 0):
                firsetPoint = [x, y]
                break
        if firsetPoint != [0, 0]:
            break
    # 往后跳50找第二个块中的参照点，50是矩形宽度
    secondPoint = [0, 0]
    for x in range(firsetPoint[0] + 50, 260):
        for y in range(0, 116):
            pixelColor = diff.getpixel((x, y))
            if pixelColor != (0, 0, 0, 0):
                secondPoint = [x, y]
                break
        if secondPoint != [0, 0]:
            break
    # #画两条线看看位置是否标注正确，仅用于调试
    draw.line((firsetPoint[0], firsetPoint[1], firsetPoint[0]+20, firsetPoint[1]),(6,255,9,0))
    draw.line((secondPoint[0], secondPoint[1], secondPoint[0]+20, secondPoint[1]),(9,8,255,0))
    diff.show()
    diffPixel = secondPoint[0] - firsetPoint[0]
    return GetStacks(diffPixel)


def GetStacks(distance):
    distance += 20
    '''
    匀加速\减速运行
        v = v0 + a * t
    位移:
    s = v * t + 0.5 * a * (t**2)
    '''
    # 初速度
    v0 = 0
    # 加减速度列表
    a_list = [3, 4, 5]
    # 时间
    t = 0.2
    # 初始位置
    s = 0
    # 向前滑动轨迹
    forward_stacks = []
    mid = distance * 3 / 5
    while s < distance:
        if s < mid:
            a = a_list[random.randint(0, 2)]
        else:
            a = -a_list[random.randint(0, 2)]
        v = v0
        stack = v * t + 0.5 * a * (t ** 2)
        # 每次拿到的位移
        stack = round(stack)
        s += stack
        v0 = v + a * t
        forward_stacks.append(stack)
    back_stacks = [-1, -1, -2, -3, -2, -3, -2, -2, -3, -1]
    return {'forward_stacks': forward_stacks, 'back_stacks': back_stacks}

# Import CrackGEE
#icon("@res:dp3vobi1-353k-q9mp-q5oh-nicth66fv4ag.png")
# UiElement.ScreenShot({"wnd":[{"cls":"Chrome_WidgetWin_1","title":"*","app":"chrome"},{"cls":"Chrome_RenderWidgetHostHWND","title":"Chrome Legacy Window"}],"html":{"tagName":"DIV","attrMap":{"tag":"DIV","aaname":"加载中...","css-selector":"body>div>div>div>div>div>div"},"index":0}},"1.png","content",{"bContinueOnError":false,"iDelayAfter":300,"iDelayBefore":200})
#icon("@res:sod4iqf0-l79e-a7hu-n4rm-hom2l8pgvv4k.png")
# Mouse.Hover({"wnd":[{"cls":"Chrome_WidgetWin_1","title":"*","app":"chrome"},{"cls":"Chrome_RenderWidgetHostHWND","title":"Chrome Legacy Window"}],"html":{"tagName":"DIV","attrMap":{"tag":"DIV","isleaf":"1","css-selector":"body>div>div>div>div>div"},"index":3}},10000,{"bContinueOnError":false,"iDelayAfter":0,"iDelayBefore":0,"bSetForeground":true,"sCursorPosition":"Center","iCursorOffsetX":0,"iCursorOffsetY":0,"sKeyModifiers":[],"sSimulate":"simulate"})
# Mouse.Click("left", "down", [])
# Delay(600)
#icon("@res:dp3vobi1-353k-q9mp-q5oh-nicth66fv4ag.png")
# UiElement.ScreenShot({"wnd":[{"cls":"Chrome_WidgetWin_1","title":"*","app":"chrome"},{"cls":"Chrome_RenderWidgetHostHWND","title":"Chrome Legacy Window"}],"html":{"tagName":"DIV","attrMap":{"tag":"DIV","aaname":"加载中...","css-selector":"body>div>div>div>div>div>div"},"index":0}},"2.png","content",{"bContinueOnError":false,"iDelayAfter":300,"iDelayBefore":200})
# dim 点列表=[]
# 点列表=CrackGEE.CalcDistance("1.png","2.png")
# TracePrint(点列表)
# Dim 正向移动=点列表["forward_stacks"]
# Dim 回移=点列表["back_stacks"]
# For Each value In 正向移动
#  Mouse.Move(value, 0, true)
# Delay(12)
# Next
# For Each value In 回移
#  Mouse.Move(value, 0, true)
# Delay(34)
# Next
# Delay(200)
# Mouse.Click("left", "up", [])


rs = CalcDistance("20200207165902.png","20200209211659.png")
print(rs)