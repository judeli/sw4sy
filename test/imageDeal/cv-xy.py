from __future__ import print_function
import cv2
import numpy as np
import argparse
import random as rng
from PIL import ImageGrab,Image

groundtruth = cv2.imread(r'20200207165902.png',0)
threshold = 100
# 使用Canny检测边缘
canny_output = cv2.Canny(groundtruth, threshold, threshold * 2)
# 找到轮廓
contours, hierarchy = cv2.findContours(canny_output, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
# 返回给定形状和类型的新数组，其中填充0。
drawing = np.zeros((canny_output.shape[0], canny_output.shape[1], 3), dtype=np.uint8)
M = cv2.moments(contours[0])  # 计算第一条轮廓的各阶矩,字典形式
center_x = int(M["m10"] / M["m00"])
center_y = int(M["m01"] / M["m00"])

color = (rng.randint(0, 256), rng.randint(0, 256), rng.randint(0, 256))
#画出轮廓
cv2.drawContours(drawing, contours, 0, color, 2, cv2.LINE_8, hierarchy, 0)
cv2.circle(drawing, (center_x, center_y), 7, 128, -1)#绘制中心点
cv2.imwrite("333.png", drawing)
#（x,y）为矩形左上角的坐标，（w,h）是矩形的宽和高
# x,y,w,h=cv2.boundingRect(contours[0])
# img=cv2.rectangle(groundtruth,(x,y),(x+w,y+h),(0,255,0),2)
# cv2.waitKey(0)
