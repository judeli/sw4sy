#coding=utf-8
import time
import win32api

import win32con
from PIL import ImageGrab

time.sleep(3)
# 参数说明
# 第一个参数 开始截图的x坐标
# 第二个参数 开始截图的y坐标
# 第三个参数 结束截图的x坐标
# 第四个参数 结束截图的y坐标
bbox = (290,212, 624,665)
im = ImageGrab.grab(bbox)

# 参数 保存截图文件的路径
im.save('I:\\pythonSolution\\projects\\sw4sy\\test\imageDeal\\sw141.png')

