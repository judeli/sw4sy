from __future__ import print_function
import cv2
import numpy as np
import argparse
import random as rng
from PIL import ImageGrab,Image

# 读取滑块图片，并给其加上相同的灰黑色透明前景色，再进行灰化
block = cv2.imread('20200209211659.png')
blockCopy = block.copy()
w, h = block.shape[:-1]
cv2.rectangle(blockCopy, (0, 0), (w, h), (47, 47, 47), -1)
cv2.addWeighted(blockCopy, 0.7, block, 0.3, 0, block)
block = cv2.cvtColor(block, cv2.COLOR_RGB2GRAY)

# 读取验证码图片，并灰化
captcha = cv2.imread('20200207165902.jpg')
captchaGray = cv2.cvtColor(captcha, cv2.COLOR_RGB2GRAY)

# 寻找captcha中匹配block的位置
res = cv2.matchTemplate(captchaGray, block, cv2.TM_SQDIFF)
min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

# 在最符合的画一个矩形
cv2.rectangle(captcha, min_loc, (min_loc[0] + w, min_loc[1] + h), (0, 0, 255), -1)

cv2.imshow('block', block)
cv2.imshow("captcha", captcha)
cv2.waitKey(0);