class Data:
    '基础数据'
    d = {}
    data_list = []
    def __init__(self, key, name, let_top_x1, let_top_y1, right_down_x2, right_down_y2, x, y, data_type):
        self.key = key
        self.name = name
        self.let_top_x1 = int(let_top_x1)
        self.let_top_y1 = int(let_top_y1)
        self.right_down_x2 = int(right_down_x2)
        self.right_down_y2 = int(right_down_y2)
        self.x = int(x)
        self.y = int(y)
        self.data_type = data_type
        tmp_d = {
            'name': self.name,
            'let_top_x1': self.let_top_x1,
            'let_top_y1': self.let_top_y1,
            'right_down_x2': self.right_down_x2,
            'right_down_y2': self.right_down_y2,
            'x': self.x,
            'y': self.y,
            'data_type': self.data_type
        }
        Data.d[key] = tmp_d
        print("当前元素个数：" + key + ":" + str(tmp_d))

    def getKey(self):
        return self.key


def getByKey(key):
    return Data.d[key]


def initData():
    print("start init data")
    tmp = Data("RightTaskEntrance", "右边任务入口", "1339", "156", "1371", "182", "1355", "170", "btn")
    Data.data_list.append(tmp)
    tmp = Data("RightTask", "右边任务按钮", "1149", "149", "1239", "191", "1198", "170", "btn")
    Data.data_list.append(tmp)
    tmp = Data("RightTaskarea", "右边任务入口", "1145", "196", "1371", "521", "0", "0", "area")
    Data.data_list.append(tmp)
    tmp = Data("RightTaskFirst", "当前任务栏第一任务", "1140", "196", "1368", "275", "1255", "248", "btn")
    Data.data_list.append(tmp)
    tmp = Data("leftEntrance", "左边入口", "19", "109", "60", "141", "38", "127", "btn")
    Data.data_list.append(tmp)
    tmp = Data("leftEntrance_transaction", "交易入口", "112", "290", "59", "353", "739", "454", "btn")
    Data.data_list.append(tmp)
    tmp = Data("leftCommonTaskEntrance", "日程入口", "20", "294", "69", "342", "43", "314", "btn")
    Data.data_list.append(tmp)
    tmp = Data("taskListarea", "任务列表", "289", "207", "570", "680", "0", "0", "area")
    Data.data_list.append(tmp)
    tmp = Data("goingDoTask", "立即传送", "919", "624", "1039", "671", "980", "649", "btn")
    Data.data_list.append(tmp)
    tmp = Data("RightTask_ShiMenClick", "师门具体任务按钮", "945", "407", "1324", "452", "1023", "434", "btn")
    Data.data_list.append(tmp)
    tmp = Data("RightTask_iWantDoShiMen", "我要做任务", "1340", "162", "1324", "452", "1023", "434", "btn")
    Data.data_list.append(tmp)
    tmp = Data("transaction_buyProduct", "逛摊", "1115", "296", "1170", "400", "1139", "348", "btn")
    Data.data_list.append(tmp)
    tmp = Data("transaction_buyProductListBtn", "摊位列表按钮", "326", "167", "436", "198", "384", "182", "btn")
    Data.data_list.append(tmp)
    tmp = Data("transaction_zb", "装备列表", "339", "292", "536", "331", "441", "313", "btn")
    Data.data_list.append(tmp)
    tmp = Data("transaction_pr", "烹饪列表", "344", "337", "543", "376", "443", "358", "btn")
    Data.data_list.append(tmp)
    tmp = Data("transaction_cy", "草药列表", "344", "380", "541", "421", "439", "404", "btn")
    Data.data_list.append(tmp)
    tmp = Data("transaction_gd", "古董列表", "344", "469", "543", "511", "441", "491", "btn")
    Data.data_list.append(tmp)
    tmp = Data("transaction_parea", "商品范围", "572", "186", "1057", "630", "0", "0", "area")
    Data.data_list.append(tmp)
    tmp = Data("topCommonTaskEntrance", "日程入口", "347", "41", "402", "100", "377", "70", "btn")
    Data.data_list.append(tmp)
    tmp = Data("topCommonTaskPanelText", "日程文字", "334", "100", "416", "142", "375", "122", "btn")
    Data.data_list.append(tmp)
    tmp = Data("topCommonTaskEntrance_shiMen", "师门任务", "330", "194", "437", "314", "381", "243", "btn")
    Data.data_list.append(tmp)
    tmp = Data("topCommonTaskEntrance_baoTu", "宝图任务", "452", "194", "562", "314", "504", "244", "btn")
    Data.data_list.append(tmp)
    tmp = Data("topCommonTaskEntrance_siChou", "丝绸之路", "571", "194", "684", "314", "626", "247", "btn")
    Data.data_list.append(tmp)
    tmp = Data("topCommonTaskEntrance_ZhuaGui", "抓鬼任务", "693", "194", "805", "314", "747", "250", "btn")
    Data.data_list.append(tmp)
    tmp = Data("topCommonTaskEntrance_JingJiCang", "竞技场", "816", "194", "925", "314", "868", "244", "btn")
    Data.data_list.append(tmp)
    tmp = Data("topCommonTaskEntrance_YingXiongSL", "英雄试炼", "938", "194", "1045", "314", "995", "265", "btn")
    Data.data_list.append(tmp)
    tmp = Data("topCommonTaskEntrance_FuBen", "副本", "330", "325", "437", "450", "380", "386", "btn")
    Data.data_list.append(tmp)
    tmp = Data("topCommonTaskEntrance_Shuangbei", "打怪双倍", "450", "325", "562", "450", "508", "382", "btn")
    Data.data_list.append(tmp)
    tmp = Data("chongwuProductList", "宠物交易", "277", "160", "408", "197", "0", "0", "text")
    Data.data_list.append(tmp)
    tmp = Data("chongwuProductList_buy", "宠物或商店购买", "965", "609", "1096", "659", "1032", "636", "btn")
    Data.data_list.append(tmp)
    tmp = Data("topCommonTaskEntrance_JingJiCang_chanllence", "竞技场-挑战", 1015,199, "0", "0", "0", "0", "btn")
    Data.data_list.append(tmp)
    return Data.d


initData()
