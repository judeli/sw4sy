from com.wws.sw4sy.common import win32, imageDeal, ocr
from com.wws.sw4sy.tasks.commonOperations import *
import time, random


# 开始装货
def click_begin_zhuanghuo_button():
    win32.click(sw4sy, 689, 549)


# 装货
def click_zhuanghuo_button():
    win32.click(sw4sy, 1033, 455)


def flow_sichouzhilu():
    if not is_open_left_task_list():
        # 打开左边任务栏
        open_left_task_list()
    time.sleep(1)
    # 先看右边任务栏是否有师门任务，有就避免使用点日程-师门的方式开启任务（人物在购买物品时，很近商店会遮挡住右边任务栏师门两个字）
    if not is_open_right_task_list():
        print("打开右边任务栏")
        open_right_task_list()
    click_task_button()
    time.sleep(1)
    click_task_sichou_button()
    time.sleep(1)
    click_begin_zhuanghuo_button()
    time.sleep(1)
    click_close_button()

    #丝绸之路本身正常情况下，买完是自动关闭交易界面的。
    #当所有栏目都不需要买的时候，需要手动关闭这个交易界面
    #可以使用确认日程界面打开成功来处理，点多一次日程，确保日程界面打开就行
    do_tanwei_buy(True, True)

    time.sleep(1)
    # 重新打开丝绸之路界面
    click_task_button()
    time.sleep(1)
    click_task_sichou_button()
    time.sleep(2)

    times = 0
    # 补偿次数
    # nextDoTimes = 0

    # 共8次装货
    while times < 16:
        print(f"start-------{times}")
        try:
            # 判断装货按钮是否存在
            x, y = imageDeal.mathc_img(973, 430, 1094, 478, workPath + "resources/img/sichou_zhuanghuo.png")
            if x == -1 and y == -1:
                print("货已装完了")
                break
        except:
            print("货已装完了2")
            break

        times += 1
        # 点击装货
        click_zhuanghuo_button()
        # 检测是否要“给予”
        # 选中给予，完成1次
        # imagebytes = imageDeal.screenshot(706, 551, 954, 594, workPath + "resources/tmp/do_gei_yu.png")
        # word = ocr.toText(imagebytes, "给予")
        # if word is not None:
        #todo 待测
        if check_is_exist_geiyu_button():
            print("进入处理给予")
            # 点击第一格
            win32.click(sw4sy, 752, 260)
            # 点击第二格
            win32.click(sw4sy, 827, 259)
            # 给与
            win32.click(sw4sy, 928, 572)
            # 继续判断是否给予成功
            # imagebytes = imageDeal.screenshot(706, 551, 954, 594, workPath + "resources/tmp/do_gei_yu.png")
            # word = ocr.toText(imagebytes, "给予")
            # if word is not None:  # 表示未成功
            if check_is_exist_geiyu_button():
                # 点击第一格
                win32.click(sw4sy, 752, 260)
                # 给与
                win32.click(sw4sy, 828, 572)
            # 检测是否买系统药，系统装备，
            # 是，点击购买
            # 结束本次循环，不计数
        else:
            print("不用给予")
            # imagebytes = imageDeal.screenshot(524,546, 793,580, workPath + "resources/tmp/do_buy_system_thing.png")
            # word = ocr.toTextContains(imagebytes, "购买")
            # if word is not None:
            #todo 待测

            if check_is_exist_system_buy_text():
                print("发现系统商店购买按钮")
                # xxx商店购买按钮，并等待走到商店处
                win32.click(sw4sy, 744,541)
                time.sleep(15)
                # # 点击购买:系统药品2个，武器、服饰3个
                # click_buy_system_thing_button()
                # time.sleep(1)
                # click_buy_system_thing_button()
                # #判断是不是系统药店，不是的话再买一个
                # if word.find("药店", 0, len(word)) < 0:
                #     print("不是药店，再买一个")
                #     click_buy_system_thing_button()

                #直到购买完成
                x, y = check_system_product_list_and_pos()
                while x != -1 and y != -1:
                    # 选中
                    # win32.click(sw4sy, x, y)
                    # 购买
                    click_buy_system_thing_button()
                    #延时大点，弹出购买成功的提示会挡住需求字眼，导致识别失败
                    time.sleep(3)
                    x, y = check_system_product_list_and_pos()

                time.sleep(1)

                # 补偿次数+1
                # nextDoTimes += 1
                # 需要重新打开装货页面
                click_task_button()
                time.sleep(1)
                click_task_sichou_button()
                time.sleep(2)
                # 点击装货
                # click_zhuanghuo_button()
                # time.sleep(1)
            else:
                print("未发现商店购买按钮")

        print(f"end-------{times}")

    # 出发
    win32.click(sw4sy, 949, 648)
    #关闭装货后界面
    click_close_button()


if __name__ == '__main__':
    win32.set_window_active(sw4sy)
    flow_sichouzhilu()
