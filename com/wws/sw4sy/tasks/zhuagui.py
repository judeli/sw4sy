from com.wws.sw4sy.common import win32, imageDeal, ocr
from com.wws.sw4sy.tasks.commonOperations import *
from com.wws.sw4sy.tasks import zudui
import time, re

def click_task_zhuagui_button():
    win32.click(sw4sy,749,250)

#我要抓鬼button
def click_start_zhuagui_button():
    win32.click(sw4sy,998,435)

# 送我回去
def click_back_to_zhuagui_button():
    win32.click(sw4sy, 990, 432)

#点击右边抓鬼任务，恢复行走
def click_right_task_list_zhuagui_text():
    win32.click(sw4sy, 1171, 219)

#一轮
def oneTurn():
    continueDo = True
    # 检测到空次数到3时，自动点击右边栏宝图任务，恢复行走
    taskTypeNoneTimes = 0
    while continueDo:

        if is_need_zai_fabu_zudui():
            print("需要重新发布组队，点确定")
            click_zudui_quren_zai_fabu()
            #这里需要等待组队完成
            print("等待组队完成")
            #TODO 重复代码
            # 循环检查组队人数，5人
            continueDo = True
            i = 0
            while continueDo:
                i += 1
                # 按enter喊话：各位稍等，组队很快
                if i % 5 == 0:
                    saySomethindInDuiwu("抓鬼任务")

                # 需要以相同组队目标:477,408,680,437
                # 需要以相同组队目标->确定:412,509,523,548
                time.sleep(1)
                renshu = check_zudui_renshu()
                print(f"当前人数：{renshu}")
                if renshu >= 5:
                    continueDo = False
                else:
                    print(f"继续组队，当前人数：{renshu}")
                #恢复行走
                if not is_open_right_task_list():
                    print("打开右边任务栏")
                    open_right_task_list()
                time.sleep(1)
                # 从组队界面恢复右边任务栏界面
                click_right_task_list_renwu_button()
                time.sleep(1)

        time.sleep(5)
        tmp = is_in_war()
        if tmp is False:
            taskTypeNoneTimes += 1
        #30秒内有没有进入打架了，判断为完成一轮
        if taskTypeNoneTimes == 3:
            continueDo = False
            print("完成一轮")

        print(f"---end---是否正在一轮继续抓鬼任务?:{continueDo}")

def zhuagui_buy():
    if not is_open_right_task_list():
        print("打开右边任务栏")
        open_right_task_list()

    time.sleep(1)
    click_right_task_list_zhuagui_text()
    #走路
    time.sleep(14)
    # 买东西（判断在逛摊界面（需要判断给予），宠物店，药品店，武器店，饰品店）
    if check_is_tanweiliebiao_page():
        taskType = "购买摊位商品"
    elif check_is_chongwudian_page():
        taskType = "宠物交易"
    elif check_is_fushidian_page():
        taskType = "买服饰"
    elif check_is_wuqidian_page():
        taskType = "买武器"
    elif check_is_yaodian_page():
        taskType = "买药品"
    # 未识别
    else:
        taskType = None

    print(f"当前任务类型:{taskType}")

    if taskType == "购买摊位商品":
        do_tanwei_buy()
        click_right_task_list_zhuagui_text()
        # 等待到达
        time.sleep(14)
        # 处理 “给予”问题
        deal_geiyu_1()
    elif taskType == "宠物交易":
        click_s_chongwu_buy_button()
        click_right_task_list_zhuagui_text()
        # 等待到达
        time.sleep(14)
        # 处理身上刚好有宠物的情况
        # TODO (身上最好不要带宝宝）只带一个宠物就行
    elif taskType == "买服饰":
        click_s_fushidian_buy_button()
        click_right_task_list_zhuagui_text()
        # 等待到达
        time.sleep(14)
    elif taskType == "买武器":
        click_s_wuqidian_buy_button()
        click_right_task_list_zhuagui_text()
        # 等待到达
        time.sleep(14)
    elif taskType == "买药品":
        click_s_yaodian_buy_button()
        click_right_task_list_zhuagui_text()
        # 等待到达
        time.sleep(14)




def flow_zhuagui():

    zudui.flow_zudui("抓鬼任务")

    print("使用点日程-师门的方式开启任务")
    click_task_button()
    time.sleep(1)
    click_task_zhuagui_button()
    time.sleep(1)
    click_start_zhuagui_button()
    time.sleep(1)

    if not is_open_right_task_list():
        print("打开右边任务栏")
        open_right_task_list()
    time.sleep(1)
    #从组队界面恢复右边任务栏界面
    click_right_task_list_renwu_button()
    time.sleep(1)
    oneTurn()
    zhuagui_buy()
    oneTurn()
    zhuagui_buy()
    oneTurn()
    zhuagui_buy()
    oneTurn()
    zhuagui_buy()

    print("抓鬼任务完成")





if __name__ == '__main__':
    # win32.set_window_active(sw4sy)
    flow_zhuagui()
    exit()




