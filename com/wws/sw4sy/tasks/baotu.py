from com.wws.sw4sy.common import win32, imageDeal, ocr
from com.wws.sw4sy.tasks.commonOperations import *
import time, re


# sw4sy = win32.get_sw4sy_win_hd()

# 打听宝图按钮
def click_queren_do_baotu_task_button():
    win32.click(sw4sy, 993, 470)


# 检测宝图任务结束
def check_baotu_do_end1():
    imagebytes = imageDeal.screenshot(955, 269, 1289, 297, workPath + "resources/tmp/tanweiliebiao_gudong_.png")
    word = ocr.toText(imagebytes, "我这里已经没有关于藏宝图的消息")
    if word is not None:
        return True
    return False

def check_baotu_do_end():
    try:
        x, y = imageDeal.mathc_img(931,264, 1345,394, workPath + "resources/img/check_baotu_do_end.png")
        if x != -1 and y != -1:
            return True
        return False
    except:
        return False

#检测右边任务列表中宝图文字，存在返回相对坐标
def check_baotu_text():
    time.sleep(2)

    try:
        x, y = imageDeal.mathc_img(1149,192, 1382, 609, workPath + "resources/img/right_task_baotu_pic.png")
        if x != -1 and y != -1:
            return (1149+x,192+y)
        else:
            time.sleep(1)
            x, y = imageDeal.mathc_img(1149, 192, 1382, 609, workPath + "resources/img/right_task_baotu_pic.png")
            if x != -1 and y != -1:
                return (1149 + x, 192 + y)
        return (-1,-1)
    except:
        return (-1,-1)

# 点击右边任务栏的宝图任务字体，启动任务
def click_right_task_list_baotu_text1():
    win32.click(sw4sy, 1171, 219)

#TODO 待测
def click_right_task_list_baotu_text(x,y):
    win32.click(sw4sy, x, y)


#用于购买商品后恢复任务，行走
def qidong_congxinqidong():
    print("使用点日程-宝图的方式恢复任务")
    click_task_button()
    time.sleep(1)
    click_task_baotu_button()


# 必须通用
# 流程：日程-宝图-走路---确认任务--什么任务？（战斗，买武器，买服饰，买药）
def flow_baotu():
    # 先看右边任务栏是否有任务，有就避免使用点日程-宝图的方式开启任务
    if not is_open_right_task_list():
        print("打开右边任务栏")
        open_right_task_list()
        time.sleep(1)
    # 看右边任务栏
    # imagebytes = imageDeal.screenshot(1149, 192, 1382, 609, workPath + "resources/tmp/right_task_list.png")
    # word = ocr.toText(imagebytes, "宝图")
    # if word is None:
    # todo 待测 宝图截图
    bx, by = check_baotu_text()
    if bx == -1 and by == -1:
        print("使用点日程-宝图的方式开启任务")
        click_task_button()
        time.sleep(1)
        click_task_baotu_button()
        time.sleep(1)
        click_queren_do_baotu_task_button()
        time.sleep(1)
        bx, by = check_baotu_text()
        print(f"使用点日程-宝图的方式开启任务,宝图坐标：{bx},{by}")
    else:
        print(f"发现右边有宝图任务,宝图坐标：{bx},{by}")
        # initTaskType = word[3:]
        click_right_task_list_baotu_text(bx,by)

    print("走路中。。。")
    time.sleep(14)

    continueDo = True
    taskType = None
    # 检测到空次数到3时，自动点击右边栏宝图任务，恢复行走
    taskTypeNoneTimes = 0
    is_first = True
    while continueDo:
        # 打架
        if is_in_war():
            taskType = "打架"
        # 买东西（判断在逛摊界面（需要判断给予），宠物店，药品店，武器店，饰品店）
        elif check_is_tanweiliebiao_page():
            taskType = "购买摊位商品"
        elif check_is_chongwudian_page():
            taskType = "宠物交易"
        elif check_is_fushidian_page():
            taskType = "买服饰"
        elif check_is_wuqidian_page():
            taskType = "买武器"
        elif check_is_yaodian_page():
            taskType = "买药品"
        # 未识别
        else:
            taskType = None

        print(f"当前任务类型:{taskType}")

        if taskType == "打架":
            if is_first:
                click_auto_button()
                is_first = False
            # 战斗中，不用管
            print("正在战斗中。。。")
            time.sleep(10)
            continue
        elif taskType == "购买摊位商品":
            do_tanwei_buy()
            bx, by = check_baotu_text()
            print(f"重新检测宝图任务坐标:{bx},{by}")
            click_right_task_list_baotu_text(bx,by)
            # 等待到达
            time.sleep(14)
            # 处理 “给予”问题
            deal_geiyu_1()
            need_wait_time_to_reach_shifu = False
        elif taskType == "宠物交易":
            click_s_chongwu_buy_button()
            bx, by = check_baotu_text()
            print(f"重新检测宝图任务坐标:{bx},{by}")
            click_right_task_list_baotu_text(bx,by)
            # 等待到达
            time.sleep(14)
            # 处理身上刚好有宠物的情况
            # TODO (身上最好不要带宝宝）只带一个宠物就行
            need_wait_time_to_reach_shifu = False
        elif taskType == "买服饰":
            click_s_fushidian_buy_button()
            bx, by = check_baotu_text()
            print(f"重新检测宝图任务坐标:{bx},{by}")
            click_right_task_list_baotu_text(bx,by)
            # 等待到达
            time.sleep(14)
        elif taskType == "买武器":
            click_s_wuqidian_buy_button()
            bx, by = check_baotu_text()
            print(f"重新检测宝图任务坐标:{bx},{by}")
            click_right_task_list_baotu_text(bx,by)
            # 等待到达
            time.sleep(14)
        elif taskType == "买药品":
            click_s_yaodian_buy_button()
            bx, by = check_baotu_text()
            print(f"重新检测宝图任务坐标:{bx},{by}")
            click_right_task_list_baotu_text(bx,by)
            # 等待到达
            time.sleep(14)
        # 走路。。。
        else:
            print("我在路上?。。。")
            taskTypeNoneTimes += 1
            if taskTypeNoneTimes == 3:
                print("恢复任务、行走")
                click_right_task_list_baotu_text(bx,by)

            time.sleep(10)

        continueDo = not check_baotu_do_end()
        print(f"---end---是否需要继续宝图任务?:{continueDo}")


if __name__ == '__main__':
    # win32.set_window_active(sw4sy)
    flow_baotu()
    # count = 0
    # for i in range(0,100):
    #     if not check_baotu_do_end():
    #         count += 1
    # print(f"识别失败次数：{count}")