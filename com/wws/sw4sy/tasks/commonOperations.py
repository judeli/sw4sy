from com.wws.sw4sy.common import win32, imageDeal, ocr
from com.wws.sw4sy.tasks import commonData
import time

sw4sy = win32.get_sw4sy_win_hd()
workPath = "I:\\pythonSolution\\projects\\sw4sy\\"


def first_do():
    win32.moveWindow2left(sw4sy)
    win32.set_window_active(sw4sy)


# 拖动验证按钮
def click_validate_button():
    win32.click(sw4sy, 445, 577)


# 关闭界面(摊位界面，药品店，饰品店，武器店）
def click_close_button():
    win32.click(sw4sy, 1130, 134)
    print("#关闭摊位界面")


# 判断左边栏打开状态
def is_open_left_task_list_3():
    imagebytes = imageDeal.screenshot(8, 146, 65, 423, workPath + "resources/tmp/left_task_list_.png")
    word = ocr.toText(imagebytes, "交易")
    if word is not None:
        print("左侧已打开")
        return True
    print("左侧未打开")
    return False


# 判断左边栏打开状态,图像匹配方式
def is_open_left_task_list_2():
    try:
        x, y = imageDeal.mathc_img(8, 146, 65, 423, workPath + "resources/img/left_task_jiaoyi.png")
        if x != -1 and y != -1:
            return True
        return False
    except:
        return False

# 判断左边栏打开状态,图像匹配方式
def is_open_left_task_list():
    try:
        x, y = imageDeal.mathc_img(18,109, 50,140, workPath + "resources/img/left_task_list_is_open_pic.png")
        if x != -1 and y != -1:
            return True
        return False
    except:
        return False


# 判断左边栏是否存在日程
def check_left_task_list_richeng():
    try:
        if not is_open_left_task_list():
            open_left_task_list()

        x, y = imageDeal.mathc_img(8, 146, 65, 423, workPath + "resources/img/left_task_richeng.png")
        if x != -1 and y != -1:
            return True
        return False
    except:
        return False


# 通过识别“”回合“”（判断战斗状态）
def is_in_war():
    try:
        x, y = imageDeal.mathc_img(67, 70, 115, 101, workPath + "resources/img/in_war.png")
        if x != -1 and y != -1:
            return True
        return False
    except:
        return False


def open_left_task_list():
    win32.click(sw4sy, 38, 131)


# 判断右边栏打开状态
def is_open_right_task_list_1():
    imagebytes = imageDeal.screenshot(1153, 148, 1241, 193, workPath + "resources/tmp/right_task_list_.png")
    word = ocr.toText(imagebytes, "任务")
    if word is not None:
        return True
    return False


#判断右边栏打开状态
def is_open_right_task_list():
    try:
        x, y = imageDeal.mathc_img(1151,150, 1372,195, workPath + "resources/img/right_task_list_is_open_pic.png")
        if x != -1 and y != -1:
            return True
        return False
    except:
        return False


# 打开右边任务栏
def open_right_task_list():
    win32.click(sw4sy, 1352, 169)


# 点击逛摊
def click_jiaoyi_guangtan_button():
    win32.click(sw4sy, 1142, 345)


# 点击摊位列表
def click_jiaoyi_tanweiliebiao_button():
    win32.click(sw4sy, 381, 181)


# 进入战斗后的，自动按钮
def click_auto_button():
    win32.click(sw4sy, 1328, 746)


# 点击摊位商品购买
def click_buy_product_area_button():
    win32.click(sw4sy, 816, 666)


# 点击摊位商品下一页或者首页按钮
def click_buy_product_area_next_page_button():
    win32.click(sw4sy, 993, 660)


# 点击系统商店购买
def click_buy_system_thing_button():
    win32.click(sw4sy, 964, 646)


# 点击交易(非战斗状态下）
def click_left_task_list_jiaoyi_button():
    win32.click(sw4sy, 35, 311)


# 点击交易(战斗状态下）
def click_left_task_list_jiaoyi_button2():
    win32.click(sw4sy, 41, 375)


# 点击装备button
def click_zhuangbei_button():
    win32.click(sw4sy, 444, 310)


# 点击烹饪button
def click_pengren_button():
    win32.click(sw4sy, 441, 358)


# 点击草药丹药button
def click_coayaodanyao_button():
    win32.click(sw4sy, 442, 406)


# 点击古董button
def click_gudong_button():
    win32.click(sw4sy, 446, 492)


# 检测装备购买需求（相隔45像素）
def check_zhuangbei_buy_need():
    try:
        # imagebytes = imageDeal.screenshot(346, 292, 541, 332, workPath + "resources/tmp/tanweiliebiao_zhuangbei_.png")
        # word = ocr.toTextContains(imagebytes, "需求")
        # if word is not None:
        gx, gy = imageDeal.mathc_img(346, 292, 541, 332, workPath + "resources/img/tanweiliebiao_xuqiu_text.png")
        if gx != -1 and gy != -1:
            return True
        return False
    except:
        return False

# 检测烹饪购买需求
def check_pengren_buy_need():
    try:
        # imagebytes = imageDeal.screenshot(346, 337, 541, 377, workPath + "resources/tmp/tanweiliebiao_pengren_.png")
        # word = ocr.toTextContains(imagebytes, "需求")
        # if word is not None:
        gx, gy = imageDeal.mathc_img(346, 337, 541, 377, workPath + "resources/img/tanweiliebiao_xuqiu_text.png")
        if gx != -1 and gy != -1:
            return True
        return False
    except:
        return False

# 检测草药丹药购买需求
def check_caoyaodanyao_buy_need():
    try:
        # imagebytes = imageDeal.screenshot(346, 382, 541, 422, workPath + "resources/tmp/tanweiliebiao_caoyaodanyao_.png")
        # word = ocr.toTextContains(imagebytes, "需求")
        # if word is not None:
        gx, gy = imageDeal.mathc_img(346, 382, 541, 422, workPath + "resources/img/tanweiliebiao_xuqiu_text.png")
        if gx != -1 and gy != -1:
            return True
        return False
    except:
        return False

# 检测古董购买需求
def check_gudong_buy_need():
    try:
        # imagebytes = imageDeal.screenshot(344, 473, 537, 509, workPath + "resources/tmp/tanweiliebiao_gudong_.png")
        # word = ocr.toTextContains(imagebytes, "需求")
        # if word is not None:
        gx, gy = imageDeal.mathc_img(344, 473, 537, 509, workPath + "resources/img/tanweiliebiao_xuqiu_text.png")
        if gx != -1 and gy != -1:
            return True
        return False
    except:
        return False


# 检测商品域需求和坐标
def check_product_list_and_pos():
    try:
        x, y = imageDeal.mathc_img(573, 187, 1050, 625, workPath + "resources/img/match_need2.png")
        if x == -1 or y == -1:
            return (-1, -1)
        return (x + 573, y + 187)
    except:
        return (-1, -1)


# 检测系统商店商品域需求和坐标
def check_system_product_list_and_pos():
    try:
        x, y = imageDeal.mathc_img(291, 216, 624, 665, workPath + "resources/img/match_need3.png")
        if x == -1 or y == -1:
            return (-1, -1)
        return (x + 291, y + 216)
    except:
        return (-1, -1)

#检测给予按钮是否存在
def check_is_exist_geiyu_button():
    try:
        x, y = imageDeal.mathc_img(706, 551, 954, 594, workPath + "resources/img/do_gei_yu_pic.png")
        if x != -1 and y != -1:
            return True
        return False
    except:
        return False


#检测是否存在“获取路径”
def check_is_exist_system_buy_text():
    try:
        x, y = imageDeal.mathc_img(514,448,794,554, workPath + "resources/img/do_buy_system_thing_pic.png")
        if x != -1 and y != -1:
            return True
        return False
    except:
        return False


# 判断是否到达商品尾页
def check_product_list_end_page():
    try:
        x, y = imageDeal.mathc_img(942, 643, 1057, 680, workPath + "resources/img/shouye.png")
        if x != -1 and y != -1:
            return True
        return False
    except:
        return False


# 判断是否需要拖动验证
def check_need_validate():
    try:
        x, y = imageDeal.mathc_img(391, 290, 942, 604, workPath + "resources/img/validateTargetPic.png")
        if x != -1 and y != -1:
            return True
        return False
    except:
        return False


# 判断是否日程界面
def check_is_richeng_page():
    try:
        x, y = imageDeal.mathc_img(626, 146, 747, 184, workPath + "resources/img/task_richeng_page.png")
        if x != -1 and y != -1:
            return True
        return False
    except:
        return False


# 判断是否摊位列表界面
def check_is_tanweiliebiao_page():
    try:
        x, y = imageDeal.mathc_img(310, 160, 433, 196, workPath + "resources/img/jiaoyi_tanweiliebiao.png")
        if x != -1 and y != -1:
            return True
        return False
    except:
        return False


# 判断是否药店界面
def check_is_yaodian_page():
    try:
        x, y = imageDeal.mathc_img(280, 160, 386, 192, workPath + "resources/img/yaodian.png")
        if x != -1 and y != -1:
            return True
        return False
    except:
        return False


# 判断是否服饰店界面
def check_is_fushidian_page():
    try:
        x, y = imageDeal.mathc_img(280, 160, 405, 190, workPath + "resources/img/fushidian.png")
        if x != -1 and y != -1:
            return True
        return False
    except:
        return False


# 判断是否武器店界面
def check_is_wuqidian_page():
    try:
        x, y = imageDeal.mathc_img(280, 160, 406, 191, workPath + "resources/img/wuqidian.png")
        if x != -1 and y != -1:
            return True
        return False
    except:
        print("有异常")
        return False


# 判断是否宠物店界面
def check_is_chongwudian_page():
    try:
        x, y = imageDeal.mathc_img(290, 160, 391, 193, workPath + "resources/img/chongwudian.png")
        if x != -1 and y != -1:
            return True
        return False
    except:
        return False


# 点击商品刷新
def click_product_list_reflush_button():
    win32.click(sw4sy, 996, 165)


# 购买从摊位列表购买
buy_tanwei_list = ("装备", "烹饪", "草药丹药", "古董")


# 装备每购买1个，就检测一下是否本次循环是否要结束
def deal_buy_zhuangbei():
    None


# 烹饪每购买2个，就检测一下是否本次循环是否要结束
def deal_buy_pengren():
    None


# 草药丹药每购买2个，就检测一下是否本次循环是否要结束
def deal_buy_caoyaodanyao():
    None


# 古董每购买1个，就检测一下是否本次循环是否要结束
def deal_buy_gudong():
    None


def default():
    print("no such type")


switch = {'装备': deal_buy_zhuangbei,  # 注意此处不要加括号
          '烹饪': deal_buy_pengren,  # 注意此处不要加括号
          '草药丹药': deal_buy_caoyaodanyao,  # 注意此处不要加括号
          '古董': deal_buy_gudong  # 注意此处不要加括号
          }


# 处理给予问题(给2个商品）
def deal_geiyu_2():
    # imagebytes = imageDeal.screenshot(706, 551, 954, 594, workPath + "resources/tmp/do_gei_yu.png")
    # word = ocr.toText(imagebytes, "给予")
    # if word is not None:
    if check_is_exist_geiyu_button():
        # 点击第一格
        win32.click(sw4sy, 752, 260)
        # 点击第二格
        win32.click(sw4sy, 827, 259)
        # 给与
        win32.click(sw4sy, 828, 572)
        # 继续判断是否给予成功
        # imagebytes = imageDeal.screenshot(706, 551, 954, 594, workPath + "resources/tmp/do_gei_yu.png")
        # word = ocr.toText(imagebytes, "给予")
        # if word is not None:  # 表示未成功
        if check_is_exist_geiyu_button():
            # 点击第一格
            win32.click(sw4sy, 752, 260)
            # 给与
            win32.click(sw4sy, 828, 572)
        return True
    return False


# 处理给予问题(给2个商品）
# 未通过
def deal_geiyu_2_use_img():
    try:
        x, y = imageDeal.mathc_img(779, 549, 874, 588, workPath + "resources/img/do_gei_yu_pic.png")
        if x != -1 and y != -1:
            # 点击第一格
            win32.click(sw4sy, 752, 260)
            # 点击第二格
            win32.click(sw4sy, 827, 259)
            # 给与
            win32.click(sw4sy, 828, 572)
            # 继续判断是否给予成功
            x, y = imageDeal.mathc_img(779, 549, 874, 588, workPath + "resources/img/do_gei_yu_pic.png")
            if x != -1 and y != -1:
                # 点击第一格
                win32.click(sw4sy, 752, 260)
                # 给与
                win32.click(sw4sy, 828, 572)
            return True
        return False
    except:
        return False


# 处理给予问题(给1个商品）
def deal_geiyu_1():
    print("处理给予1")
    # imagebytes = imageDeal.screenshot(706, 551, 954, 594, workPath + "resources/tmp/do_gei_yu.png")
    # word = ocr.toText(imagebytes, "给予")
    # if word is not None:
    if check_is_exist_geiyu_button():
        # 点击第一格
        win32.click(sw4sy, 752, 260)
        # 给与
        win32.click(sw4sy, 828, 572)
        return True
    return False


# 处理给予问题(给1个商品）
# 未通过
def deal_geiyu_1_use_img():
    try:
        print("处理给予1-匹配图片")
        # x, y = imageDeal.mathc_img(779, 549, 874, 588, workPath + "resources/img/do_gei_yu_pic.png")
        # if x != -1 and y != -1:
        if check_is_exist_geiyu_button():
            # 点击第一格
            win32.click(sw4sy, 752, 260)
            # 给与
            win32.click(sw4sy, 928, 572)
            return True
        return False
    except:
        return False


def do_tanwei_buy(need_check_left_task_list=False,
                  need_click_left_task_list_jiaoyi_button=False,
                  need_click_close=False):
    if need_check_left_task_list:
        # 打开左边任务栏
        if not is_open_left_task_list():
            open_left_task_list()
    if need_click_left_task_list_jiaoyi_button:
        # 点击交易
        click_left_task_list_jiaoyi_button()

    time.sleep(1)
    # 点击逛摊
    click_jiaoyi_guangtan_button()
    time.sleep(1)
    # 点击摊位列表
    click_jiaoyi_tanweiliebiao_button()

    # 循环
    for buyType in buy_tanwei_list:
        current_buy_type = buyType
        tmp_buy_type = None
        if current_buy_type == "装备":
            current_buy_need = check_zhuangbei_buy_need()
        elif current_buy_type == "烹饪":
            current_buy_need = check_pengren_buy_need()
        elif current_buy_type == "草药丹药":
            current_buy_need = check_caoyaodanyao_buy_need()
        elif current_buy_type == "古董":
            current_buy_need = check_gudong_buy_need()

        if current_buy_need is False:
            print(f"不需要购买{current_buy_type}:{current_buy_need}")
            continue

        # 记录购买次数
        buy_times = 0
        while current_buy_need:
            # 首次循环
            if tmp_buy_type is None:
                tmp_buy_type = current_buy_type

                if current_buy_type == "装备":
                    click_zhuangbei_button()
                elif current_buy_type == "烹饪":
                    click_pengren_button()
                elif current_buy_type == "草药丹药":
                    click_coayaodanyao_button()
                elif current_buy_type == "古董":
                    click_gudong_button()

            # 在商品域查找‘需求’，同时需要坐标
            # 商品域坐标：573,187，1050,625
            x, y = check_product_list_and_pos()
            print(f"需求商品坐标：{x},{y}")
            if x != -1 and y != -1:
                # 选中
                win32.click(sw4sy, x, y)
                time.sleep(1)
                # 购买
                click_buy_product_area_button()
                time.sleep(1)
                buy_times += 1
                print(f"正在购买{current_buy_type},次数{buy_times}")

                if current_buy_type == "装备":
                    current_buy_need = check_zhuangbei_buy_need()
                elif current_buy_type == "烹饪":
                    current_buy_need = check_pengren_buy_need()
                elif current_buy_type == "草药丹药":
                    current_buy_need = check_caoyaodanyao_buy_need()
                elif current_buy_type == "古董":
                    current_buy_need = check_gudong_buy_need()

                time.sleep(1)
                print(f"是否需要继续购买{current_buy_type}:{current_buy_need}")
            else:
                print("本页找不到需求商品")
                # 达到尾页，进行刷新，才继续翻页
                if check_product_list_end_page():
                    # 刷新商品（自动回到第一页）
                    click_product_list_reflush_button()
                    print("到达尾页，刷新")
                else:
                    # 不需要刷新就点下一页
                    click_buy_product_area_next_page_button()
                    print("进入下一页")
                time.sleep(1)

    # 虽然买完会自动关闭。如果重新运行（买完后），不关这个界面就导致没打开交易
    if need_click_close:
        click_close_button()
    return


# 宠物店-购买
def click_s_chongwu_buy_button():
    win32.click(sw4sy, 1032, 636)


# 武器店-购买
def click_s_wuqidian_buy_button():
    win32.click(sw4sy, 964, 646)


# 服饰店-购买
def click_s_fushidian_buy_button():
    win32.click(sw4sy, 964, 646)


# 药店-购买
def click_s_yaodian_buy_button():
    win32.click(sw4sy, 964, 646)


# 系统商店-购买
def click_system_shangdian_buy_button():
    win32.click(sw4sy, 964, 646)


def do_system_buy():
    None


# 系统药店每购买2个，就检测一下是否本次循环是否要结束
# 系统武器店每购买3个，就检测一下是否本次循环是否要结束
# 系统服饰店每购买3个，就检测一下是否本次循环是否要结束


# 日程
def click_task_button():
    win32.click(sw4sy, 375, 69)
    print("点日程")
    time.sleep(1)
    if not check_is_richeng_page():
        win32.click(sw4sy, 375, 69)
        print("-----再次点日程----")


# 日常-师门
def click_task_shimen_button():
    win32.click(sw4sy, 383, 248)


# 日常-宝图
def click_task_baotu_button():
    tmp = commonData.getByKey("topCommonTaskEntrance_baoTu")
    win32.click(sw4sy, tmp['x'], tmp['y'])


# 日常-丝绸
def click_task_sichou_button():
    tmp = commonData.getByKey("topCommonTaskEntrance_siChou")
    print(str(tmp))
    win32.click(sw4sy, tmp['x'], tmp['y'])
    print("点丝绸")


# 日常-竞技
def click_task_jingji_button():
    tmp = commonData.getByKey("topCommonTaskEntrance_JingJiCang")
    print(str(tmp))
    win32.click(sw4sy, tmp['x'], tmp['y'])


# 点击右边任务button
def click_right_task_list_renwu_button():
    win32.click(sw4sy, 1196, 169)


# taskType:野外打怪，抓鬼任务，野外封妖
def saySomethindInDuiwu(taskType):
    base = "速度了，满人即开"
    say = None
    if taskType == "野外打怪":
        say = "挂野150," + base
    elif taskType == "抓鬼任务":
        say = "40鬼," + base
    elif taskType == "野外封妖":
        say = "手动封妖20," + base

    win32.setText(say)

    win32.k.tap_key(win32.k.enter_key)

    # 创建组合键
    win32.k.press_key(win32.k.control_key)
    win32.k.tap_key('v')
    win32.k.release_key(win32.k.control_key)

    win32.k.tap_key(win32.k.enter_key)


# 判断是否需要按确定，重新发布组队
def is_need_zai_fabu_zudui():
    try:
        x, y = imageDeal.mathc_img(412, 509, 523, 548, workPath + "resources/img/zudui_queding_zai_fabu.png")
        if x != -1 and y != -1:
            return True
        return False
    except:
        return False


# 点击确认重新发布组队
def click_zudui_quren_zai_fabu():
    win32.click(sw4sy, 474, 530)


# 检测组队人数
#TODO 如何使用非OCR方式解决？
def check_zudui_renshu():
    imagebytes = imageDeal.screenshot(1206, 197, 1369, 496, workPath + "resources/tmp/zudui_renshu.png")
    list = ocr.toTextList(imagebytes)
    if list is not None:
        return len(list)
    return 0

if __name__ == '__main__':
    # first_do()
    # print(check_zhuangbei_buy_need())
    # print(check_pengren_buy_need())
    # print(check_caoyaodanyao_buy_need())
    # print(check_gudong_buy_need())
    print(check_is_exist_system_buy_text())