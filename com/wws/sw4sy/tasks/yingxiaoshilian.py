from com.wws.sw4sy.common import win32, imageDeal, ocr
from com.wws.sw4sy.tasks.commonOperations import *
import time, random


def click_task_yingxiongshilian_button():
    win32.click(sw4sy, 994, 262)


# 接受试炼
def click_todo_yingxiongshilian_button():
    win32.click(sw4sy, 994, 447)


# 挑战按钮
def click_todo_yingxiongshilian_tiaozhan_button():
    win32.click(sw4sy, 581, 698)


# 判断是否通关
# cx,cy为方框中心点
#测试未通过
def check_is_yingxiongshilian_tongguan(cx, cy):
    try:
        print(f"输入：{cx},{cy}--->{cx - 40},{cy - 40}，{cx + 40},{cy + 40}")
        x, y = imageDeal.mathc_img(cx - 40, cy - 40, cx + 40, cy + 40,
                                   workPath + "resources/img/tongguan.png")
        if x != -1 and y != -1:
            return True
        return False
    except:
        return False


# 判断地图-----------#测试未通过
def to_match_ditu(weekDay,targetPicPath):
    try:
        x1,y1,x2,y2 = click_dict_area[weekDay]
        print(f"截图：{x1},{y1},{x2},{y2}")
        x, y = imageDeal.mathc_img(x1,y1,x2,y2,targetPicPath)
        if x != -1 and y != -1:
            return True
        return False
    except:
        return False

#测试未通过
def match_ditu(weekDay):
    picNames = ["shilian_donghailonggong",
                "shilian_gaolaozhuang",
                "shilian_huaguoshan",
                "shilian_huoyanshan",
                "shilian_liushahe",
                "shilian_shituoling",
                "shilian_tianting"
                ]

    for name in picNames:
        path = workPath+"resources/img/"+name+".png"
        print(path)
        if to_match_ditu(weekDay, path):
            if name == "shilian_donghailonggong":
                return "东海龙宫"
            if name == "shilian_gaolaozhuang":
                return "高老庄"
            if name == "shilian_huaguoshan":
                return "花果山"
            if name == "shilian_huoyanshan":
                return "火焰山"
            if name == "shilian_liushahe":
                return "流沙河"
            if name == "shilian_shituoling":
                return "狮驼岭"
            if name == "shilian_tianting":
                return "天庭"
    return None

 #   ---------------------


def click_close_shilian_page():
    win32.click(sw4sy, 1324, 752)

#领奖(后面可能有入侵
#TODO 暂时定屏幕中间
def click_shilian_jiangli():
    win32.click(sw4sy,662,526)

#打奖牌
def click_shilian_jiangli_paizi():
    win32.click(sw4sy, 1319,471)

#领取周奖励
def click_shilian_zhou_jiangli():
    win32.click(sw4sy,594,72)

# 周1-7点击的
click_dict = {
    1: (92, 69),
    2: (164, 69),
    3: (241, 69),
    4: (307, 69),
    5: (388, 69),
    6: (459, 69),
    7: (526, 69)
}
# 周1-7点击的图标区域
click_dict_area = {
    1: (59,38,126,98),
    2: (129,36,200,95),
    3: (208,39,278,100),
    4: (279,38,348,95),
    5: (350,38,421,98),
    6: (422,39,496,101),
    7: (494,40,559,99)
}
# 对应地图的每个点击点
click_dict_points = {
    #505,470,536,501
    "火焰山": ((490, 427), (589,565), (766,665), (987,576), (961,448)),
    "花果山": ((400,564), (561,620), (558,452), (798,592), (931,450)),
    "高老庄": ((513,468), (622,599), (984,674), (1049,549), (923,426)),
    "流沙河": ((465,363), (379,483), (511,573), (652,624), (798,440)),
    "天庭": ((470,196), (426,367), (593,435), (1057,226), (830,165)),
    "狮驼岭": ((450,361), (530,540), (881,608), (920,499), (838,359)),
    "东海龙宫": ((703,463), (681,596), (889,664), (1110,553), (989,383))
}
#中心图位置
# click_dict_center_points = {
#     "东海龙宫": (881,494),
#     "花果山": (691,441),
#     "高老庄": (711,424),
#     "流沙河": (608,435),
#     "天庭": (693,302),
#     "狮驼岭": (630,420),
#     "火焰山": (695,416)
# }
#缩到最小的时候，位置
# click_dict_center_points = {
#     "东海龙宫": (1092,654),
#     "花果山": (990,431),
#     "高老庄": (609,560),
#     "流沙河": (285,601),
#     "天庭": (332,181),
#     "狮驼岭": (299,406),
#     "火焰山": (658,287)
# }
click_center_points = [
    (609,560,"高老庄"),
    (990,431,"花果山"),
    (299,406,"狮驼岭"),
    (332,181,"天庭"),
     (658,287,"火焰山"),
     (285,601,"流沙河"),
     (1134,637,"东海龙宫")
]

#入侵点
ruqin_dict_points = {
    "东海龙宫": (),
    "花果山": (807,567),
    "高老庄": (),
    "流沙河": (),
    "天庭": (),
    "狮驼岭": (),
    "火焰山": ()
}
#宝箱奖励点
baoxiang_dict_points = {
    "东海龙宫": (),
    "花果山": (),
    "高老庄": (),
    "流沙河": (),
    "天庭": (),
    "狮驼岭": (),
    "火焰山": (809,513)
}

def flow_yingxiongshilian():
    click_task_button()
    time.sleep(1)
    click_task_yingxiongshilian_button()
    time.sleep(1)
    click_todo_yingxiongshilian_button()
    time.sleep(3)
    localtime = time.localtime(time.time())
    weekday_index = int(localtime[6])
    print(f"index : {weekday_index}")
    #缩到最小
    win32.mouse_wheel_down_4(sw4sy)
    time.sleep(1)
    # x, y = click_dict[weekday_index]
    # win32.click(sw4sy, x, y)
    # time.sleep(4)
    #
    # ditu_name = match_ditu(weekDay=weekday_index)
    # print(f"匹配到地图为:{ditu_name}")
    # 点击地图中心

    p = click_center_points[weekday_index]
    win32.click(sw4sy, p[0], p[1])
    time.sleep(4)

    # 确定选择地图
    win32.click(sw4sy, 582, 699)
    time.sleep(1)

    #t退出试炼重新进入，以使选中的地图自动居中
    # click_close_shilian_page()
    # time.sleep(2)
    # click_task_button()
    # time.sleep(1)
    # click_task_yingxiongshilian_button()
    # time.sleep(1)
    # click_todo_yingxiongshilian_button()
    # time.sleep(2)

    points = click_dict_points[p[2]]
    is_fisrt = True
    for cx, cy in points:

        print("--------------start-----------------")
        print(f"挑战点：{cx},{cy}")
        #选中挑战点
        win32.click(sw4sy, cx, cy)
        time.sleep(1.2)
        #挑战
        click_todo_yingxiongshilian_tiaozhan_button()
        time.sleep(2)
        #检测是否进入了战斗，没有表示已经挑战过，进入下一个挑战点
        if not is_in_war():
            #防止提示实力上升
            # 选中挑战点
            win32.click(sw4sy, cx, cy)
            time.sleep(2)
            # 挑战
            click_todo_yingxiongshilian_tiaozhan_button()
            time.sleep(2)
            if not is_in_war():
                print(f"挑战点：{cx},{cy}已经通关，进入下一个挑战点")
                continue
        #注意此处坐标也是退出的坐标
        click_auto_button()
        time.sleep(1)
        # 10秒检测左边日程消失
        continueDo = True
        while continueDo:
            time.sleep(3)
            if not is_in_war():
                break
        print("--------------end-----------------")

    #检测奖牌
    click_shilian_jiangli_paizi()
    time.sleep(1)
    # 挑战
    click_todo_yingxiongshilian_tiaozhan_button()
    time.sleep(2)
    if is_in_war():
        click_auto_button()
        #等待打完
        while is_in_war():
            time.sleep(10)
    #领奖励
    click_shilian_jiangli()
    time.sleep(1)
    #检测入侵
    #TODO

    #试图领取周奖励,周日
    if weekday_index == 6:
        click_shilian_zhou_jiangli()

    print("*****试炼完成******")

    click_close_shilian_page()
    time.sleep(1)


if __name__ == '__main__':
    first_do()
    # win32.mouse_wheel_down_4(sw4sy)
    flow_yingxiongshilian()
    exit()
