from com.wws.sw4sy.common import win32, imageDeal, ocr
from com.wws.sw4sy.tasks.commonOperations import *
import time, re


def click_right_task_list_duiwu():
    win32.click(sw4sy, 1289, 170)


def click_create_duiwu():
    win32.click(sw4sy, 369, 185)


# 选择任务目标
# 分别野外打怪，抓鬼任务，野外封妖
task_targets = ((633,267), (630,336), (629,547))


def select_task_target(x, y):
    win32.click(sw4sy, x, y)

#taskType:野外打怪，抓鬼任务，野外封妖
def flow_zudui(taskType):
    # 先看右边任务栏是否有任务，有就避免使用点日程-宝图的方式开启任务
    if not is_open_right_task_list():
        print("打开右边任务栏")
        open_right_task_list()
        time.sleep(1)
    print("点击右边任务栏队伍")
    click_right_task_list_duiwu()
    time.sleep(1)
    print("创建队伍")
    click_create_duiwu()
    time.sleep(1)

    if taskType == "野外打怪":
        x,y = task_targets[0]
        select_task_target(x,y)
    elif taskType == "抓鬼任务":
        x, y = task_targets[1]
        select_task_target(x,y)
        # 自动跨服
        win32.click(sw4sy, 977, 563)
    elif taskType == "野外封妖":
        x, y = task_targets[2]
        select_task_target(x,y)

    time.sleep(1)
    # 发布
    win32.click(sw4sy, 1031,652)
    time.sleep(0.2)
    click_close_button()
    time.sleep(1)

    #循环检查组队人数，5人
    continueDo = True
    i = 0
    while continueDo:
        i += 1
        #按enter喊话：各位稍等，组队很快
        if i % 5 == 0:
            saySomethindInDuiwu(taskType)

        # 需要以相同组队目标:477,408,680,437
        # 需要以相同组队目标->确定:412,509,523,548
        time.sleep(1)
        if is_need_zai_fabu_zudui():
            print("需要重新发布组队，点确定")
            click_zudui_quren_zai_fabu()
        time.sleep(1)
        renshu = check_zudui_renshu()
        print(f"当前人数：{renshu}")
        if renshu >= 5:
            continueDo = False
        else:
            print(f"继续组队，当前人数：{renshu}")

    print(f"完成组队人数：{renshu}")





