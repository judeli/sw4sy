from com.wws.sw4sy.common import win32, imageDeal, ocr
from com.wws.sw4sy.tasks.commonOperations import *
import time, random

tiaozhan_list_button = ((1053, 213), (1055, 303), (1054, 396), (1054, 490), (1054, 585))


def click_tiaozhan_button():
    x, y = random.choice(tiaozhan_list_button)
    print(f"挑战坐标：（{x}:{y}）")
    win32.click(sw4sy, x, y)


def flow_jingji():
    click_task_button()
    time.sleep(1)

    times = 0
    imagebytes = imageDeal.screenshot(816,277, 904,307,
                                      "I:\\pythonSolution\\projects\\sw4sy\\resources\\tmp/tiaozhancishu.png")
    word = ocr.toText(imagebytes, "次数")
    if word is not None:
        times = int(word[3])
        print(f"初始化次数：{times}")
    click_task_jingji_button()
    time.sleep(1)

    while times < 8:
        times += 1
        print(f"竞技第{times}次")
        #TODO 加防误判（判断是否是竞技界面）
        click_tiaozhan_button()
        time.sleep(1)
        #防止弹出装备增强提示界面导致失败
        click_tiaozhan_button()
        time.sleep(1)
        click_auto_button()

        # 每10秒检测结束（等待出现挑战）
        exceptionCheck = 0
        while True:
            time.sleep(6)
            if not is_in_war():
                break
            print("竞技战斗中。。。")
    print("竞技结束")

"""初步测试OK"""
if __name__ == '__main__':
    win32.set_window_active(sw4sy)
    flow_jingji()
