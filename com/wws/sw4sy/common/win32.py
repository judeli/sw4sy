import win32gui, win32api, win32con, time
#python win32api win32gui win32con PyUserInput实现自动化脚本
from pymouse import PyMouse
from pykeyboard import PyKeyboard
import win32clipboard as w

import time
# 实例化
m = PyMouse()
k = PyKeyboard()

def mouse_wheel_down_4(whnd):
    set_window_active(whnd)
    time.sleep(0.5)
    # win32api.mouse_event(win32con.MOUSEEVENTF_WHEEL, 1, 1, -4)
    # win32api.SendMessage(whnd,win32con.MOUSEEVENTF_WHEEL, 0, -4)
    # win32gui.SendMessage(whnd, win32con.MOUSEEVENTF_WHEEL, 0,0, -2)
    m.click(100,100)
    time.sleep(0.5)
    m.scroll(-15,0)

#同时会激活窗口
def click(whnd, x, y):
    time.sleep(0.5)
    # sx, sy = win32gui.ScreenToClient(whnd, (x, y))
    sx = x
    sy = y
    print(f"输入坐标：{x}:{y},修正坐标:{sx}:{sy}")
    # 鼠标点击 参数:x,y,button=1(左键)、2(右键)、3(中间),次数
    m.click(sx, sy)
    time.sleep(0.5)

#警告，估计引起了封号检测
def click1(whnd, x, y):
    #转换为窗口坐标
    sx, sy = win32gui.ScreenToClient(whnd, (x, y))
    print(f"输入坐标：{x}:{y},修正坐标:{sx}:{sy}")
    # lParam = win32api.MAKELONG(x-10, y-30)
    lParam = win32api.MAKELONG(sx, sy)
    # MOUSEMOVE event is required for game to register clicks correctly
    rs = win32gui.SendMessage(whnd, win32con.WM_MOUSEMOVE, 0, lParam)
    time.sleep(0.3)
    rs = win32gui.SendMessage(whnd, win32con.WM_LBUTTONDOWN, win32con.MK_LBUTTON, lParam)
    time.sleep(0.05)
    rs = win32gui.SendMessage(whnd, win32con.WM_LBUTTONUP, win32con.MK_LBUTTON, lParam)
    time.sleep(1)

hwnd_title = dict()

def get_all_hwnd(hwnd, mouse):
    if win32gui.IsWindow(hwnd) and win32gui.IsWindowEnabled(hwnd) and win32gui.IsWindowVisible(hwnd):
        hwnd_title.update({hwnd: win32gui.GetWindowText(hwnd)})


def get_sw4sy_win_hd():
    # python3 遍历windows下 所有句柄及窗口名称
    win32gui.EnumWindows(get_all_hwnd, 0)
    sw4sy_win_hd = None
    for h, t in hwnd_title.items():
        if t is not "":
            print(h, t)
            if t.startswith("神武4手游"):
                sw4sy_win_hd = h
    return sw4sy_win_hd


def set_window_active(sw4sy_win_hd):
    # win32gui.SendMessage(sw4sy_win_hd, win32con.WM_ACTIVATE, win32con.WA_ACTIVE, 0)
    win32gui.SetForegroundWindow(sw4sy_win_hd)
    # 获取标题
    title = win32gui.GetWindowText(sw4sy_win_hd)  # jbid为句柄id
    print("标题：%s" % title)
    # 获取类名: GLFW30
    clsname = win32gui.GetClassName(sw4sy_win_hd)
    print("类名：%s" % clsname)

def moveWindow2left(sw4sy_win_hd):
    # 分别为左、上、右、下的窗口位置
    left, top, right, bottom = win32gui.GetWindowRect(sw4sy_win_hd)
    print("窗口位置：%d,%d,%d,%d" % (left, top, right, bottom))
    # 移动窗口到左上角
    # 没有直接修改窗口大小的方式，但可以曲线救国，几个参数分别表示句柄,起始点坐标,宽高度,是否重绘界面
    # ，如果想改变窗口大小，就必须指定起始点的坐标，没果对起始点坐标没有要求，随便写就可以；如果还想要放在原先的位置，
    # 就需要先获取之前的边框位置，再调用该方法即可
    win32gui.MoveWindow(sw4sy_win_hd, 0, 0, right - left, bottom - top, True)
    left, top, right, bottom = win32gui.GetWindowRect(sw4sy_win_hd)
    print("移动后窗口位置：%d,%d,%d,%d" % (left, top, right, bottom))


def getText():
    # 读取剪切板
    w.OpenClipboard()
    d = w.GetClipboardData(win32con.CF_TEXT)
    w.CloseClipboard()
    return d


def setText(aString):
    # 写入剪切板
    w.OpenClipboard()
    w.EmptyClipboard()
    w.SetClipboardData(win32con.CF_TEXT, aString.encode(encoding='utf-8'))
    w.CloseClipboard()


def click_position(hwd, x_position, y_position, sleep):
  """
  鼠标左键点击指定坐标
  :param hwd:
  :param x_position:
  :param y_position:
  :param sleep:
  :return:
  """
  # 将两个16位的值连接成一个32位的地址坐标
  long_position = win32api.MAKELONG(x_position, y_position)
  # win32api.SendMessage(hwnd, win32con.MOUSEEVENTF_LEFTDOWN, win32con.MOUSEEVENTF_LEFTUP, long_position)
  # 点击左键
  win32api.SendMessage(hwd, win32con.WM_LBUTTONDOWN, win32con.MK_LBUTTON, long_position)
  win32api.SendMessage(hwd, win32con.WM_LBUTTONUP, win32con.MK_LBUTTON, long_position)
  time.sleep(int(sleep))